# IAGENT

接口注解dsa声明式HTTP请求，针对大部分对接第三方接口场景，便于统一管理第三方接口。

注：当前版本已经进入到Beta.2阶段，推荐使用1.2.1以上版本的依赖，详细版本号列表：https://gitee.com/LiuJieYuuuu/iagent-parent/wikis/%E9%99%84%E5%BD%95/%E9%99%84%E5%BD%95%E4%BA%8C%20-%20%E7%89%88%E6%9C%AC%E8%BF%AD%E4%BB%A3

## 简单入门

### 使用Iagent

#### Maven

```xml
<dependency>
    <groupId>com.gitee.liujieyuuuu</groupId>
    <artifactId>iagent</artifactId>
    <version>2.2.1</version>
</dependency>
```

#### 代码示例

* 接口类

  ```java
  @IagentUrl(value = "http://127.0.0.1:8999/fegin")
  public interface IagentDao {
  
      @IagentUrl(value = "/getAsyncUserInfo")
      Map<String, Object> getAsyncUserInfo();
  
  }
  ```

* 使用示例

  ```Java
  // 创建全局配置
  IagentConfiguration configuration = new IagentConfiguration();
  // 扫描接口包路径
  configuration.setBasePackages(new String[]{"org.example.idao"});
  // 创建工厂
  IagentFactory iagentFactory = new DefaultIagentFactory(configuration);
  // 获取到接口代理类
  IagentDao iagentDao = iagentFactory.getProxy(IagentDao.class);
  // 开始调用方法
  Map<String, Object> result = iagentDao.getAsyncUserInfo();
  System.out.println("result:" + result);
  ```

* 执行结果

  ```
  result:{name=Netty, id=123, timestamp=1662554680100}
  ```

  

### Iagent集成Spring

基于Spring 5.1.19版本

#### Maven

```xml
<dependency>
    <groupId>com.gitee.liujieyuuuu</groupId>
    <artifactId>iagent</artifactId>
    <version>2.2.1</version>
</dependency>
<dependency>
    <groupId>com.gitee.liujieyuuuu</groupId>
    <artifactId>iagent-spring</artifactId>
    <version>1.2.1</version>
</dependency>
```

#### 代码示例

* Spring配置

  ```java
  @IagentComponentScan(value = "org.example.idao")
  @Configuration
  public class IagentInfoConfig  {
      
  }
  ```

* 使用示例

  ```java
  @Autowired
  IagentDao iagentDao;
  
  @Test
  public void contextTest() {
  	Map<String, Object> result = iagentDao.getAsyncUserInfo();
  	System.out.println("result:" + result);
  }
  ```

* 执行结果

  ```
  result:{name=Netty, id=123, timestamp=1662554680100}
  ```

  

### Iagent集成SpringBoot

基于Spring Boot 2.1.18版本

#### Maven

```xml
<dependency>
    <groupId>com.gitee.liujieyuuuu</groupId>
    <artifactId>iagent-spring-boot-starter</artifactId>
    <version>1.2.1</version>
</dependency>
```

#### 使用示例

* SpringBoot配置

  ```java
  @EnabledIagent(value = {"org.example.idao"})
  @SpringBootApplication
  public class SpringBootApp {
  
      public static void main(String[] args) {
          SpringApplication.run(SpringBootApp.class, args);
      }
  
  }
  ```

* 使用示例

  ```java
  @Autowired
  IagentDao iagentDao;
  
  @Test
  public void contextTest() {
  	Map<String, Object> result = iagentDao.getAsyncUserInfo();
  	System.out.println("result:" + result);
  }
  ```

* 执行结果

  ```
  result:{name=Netty, id=123, timestamp=1662554680100}
  ```


## 详细说明文档

https://gitee.com/LiuJieYuuuu/iagent-parent/wikis/%E6%9B%B4%E6%96%B0%E8%AF%B4%E6%98%8E%E8%AE%B0%E5%BD%95/2.2.1%E7%89%88%E6%9C%AC%E6%9B%B4%E6%96%B0%E8%AF%B4%E6%98%8E%EF%BC%88%E5%9F%BA%E4%BA%8E2.2.0%E8%BF%AD%E4%BB%A3%EF%BC%89