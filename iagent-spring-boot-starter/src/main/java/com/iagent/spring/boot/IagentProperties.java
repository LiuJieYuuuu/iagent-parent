package com.iagent.spring.boot;

import com.iagent.constant.HttpConstant;
import com.iagent.constant.HttpEnum;
import com.iagent.json.JSONSupport;
import com.iagent.logging.Logger;
import com.iagent.plugins.Interceptor;
import com.iagent.request.HttpExecutor;
import com.iagent.resovler.parameter.ParameterResolver;
import com.iagent.resovler.result.ResultResolver;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "iagent")
public class IagentProperties {

    /**
     * set log adapter
     */
    private Class<? extends Logger> logImpl;

    /**
     * set JSON adapter
     */
    private Class<? extends JSONSupport> jsonSupport;

    private RequestConfig requestConfig;

    /**
     * add result parser
     * @since 1.1.0
     */
    private List<Class<? extends ResultResolver>> addResultResolvers;

    /**
     * add parameter resolver
     * @since 1.1.0
     */
    private List<Class<? extends ParameterResolver>> addParameterResolvers;

    /**
     * add interceptor class
     * @since 1.1.0
     */
    private List<Class<? extends Interceptor>> plugins;

    /**
     * The switch of banner printer
     * @since 1.1.1
     */
    private Boolean bannerMode;

    public Class<? extends Logger> getLogImpl() {
        return logImpl;
    }

    public void setLogImpl(Class<? extends Logger> logImpl) {
        this.logImpl = logImpl;
    }

    public Class<? extends JSONSupport> getJsonSupport() {
        return jsonSupport;
    }

    public void setJsonSupport(Class<? extends JSONSupport> jsonSupport) {
        this.jsonSupport = jsonSupport;
    }

    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    public void setRequestConfig(RequestConfig requestConfig) {
        this.requestConfig = requestConfig;
    }

    public List<Class<? extends ResultResolver>> getAddResultResolvers() {
        return addResultResolvers;
    }

    public void setAddResultResolvers(List<Class<? extends ResultResolver>> addResultResolvers) {
        this.addResultResolvers = addResultResolvers;
    }

    public List<Class<? extends ParameterResolver>> getAddParameterResolvers() {
        return addParameterResolvers;
    }

    public void setAddParameterResolvers(List<Class<? extends ParameterResolver>> addParameterResolvers) {
        this.addParameterResolvers = addParameterResolvers;
    }

    public List<Class<? extends Interceptor>> getPlugins() {
        return plugins;
    }

    public void setPlugins(List<Class<? extends Interceptor>> plugins) {
        this.plugins = plugins;
    }

    public Boolean getBannerMode() {
        return bannerMode;
    }

    public void setBannerMode(Boolean bannerMode) {
        this.bannerMode = bannerMode;
    }

    public static class RequestConfig {

        /**
         * Default request type
         */
        private HttpEnum requestType = HttpEnum.GET;

        /**
         * default content type
         */
        private String contentType = HttpConstant.X_WWW_FORM_URLENCODED;

        /**
         * default connection time
         */
        private int connectionTime = HttpConstant.CONNECTION_TIME;

        /**
         * default read time
         */
        private int readTime = HttpConstant.READ_TIME;

        /**
         * default http executor
         */
        private Class<? extends HttpExecutor> httpExecutor = HttpExecutor.DEFAULT_EXECUTOR;

        public HttpEnum getRequestType() {
            return requestType;
        }

        public void setRequestType(HttpEnum requestType) {
            this.requestType = requestType;
        }

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public int getConnectionTime() {
            return connectionTime;
        }

        public void setConnectionTime(int connectionTime) {
            this.connectionTime = connectionTime;
        }

        public int getReadTime() {
            return readTime;
        }

        public void setReadTime(int readTime) {
            this.readTime = readTime;
        }

        public Class<? extends HttpExecutor> getHttpExecutor() {
            return httpExecutor;
        }

        public void setHttpExecutor(Class<? extends HttpExecutor> httpExecutor) {
            this.httpExecutor = httpExecutor;
        }
    }

}
