package com.iagent;

import com.iagent.json.JSON;
import com.iagent.json.gson.GsonSupport;
import com.iagent.json.jackson.JacksonSupport;
import org.junit.Test;

public class TestIagent {

    @Test
    public void test() {
        String data = "11111";
        JSON.useJson(GsonSupport.class, true);
        long time1 = System.nanoTime();
        System.out.println(Short.parseShort(data));
        long time2 = System.nanoTime();
        System.out.println("time:" + 1.0 * (time2 - time1)/1000000);
        Short jsonObject = JSON.getJSONObject(data, short.class);
        long time3 = System.nanoTime();
        System.out.println("time:" + 1.0 * (time3 - time2)/1000000);
        System.out.println(jsonObject);
    }

}
