package com.iagent;

import com.iagent.util.ClassUtils;
import org.junit.Test;

import java.lang.reflect.Method;

/**
 * @author liujieyu
 * @date 2023/1/19 10:09
 * @desciption
 */
public class TestClassUtils {
    @Test
    public void context() {
        try {
            Method getMethodName = ClassUtils.class.getMethod("getMethodName", Method.class);
            System.out.println(String.valueOf(getMethodName.hashCode()));
            long time = System.nanoTime();
            for (int i = 0; i < 10000; i++) {
                String.valueOf(getMethodName.hashCode());
            }
            System.out.println(1.0 * (System.nanoTime() - time) / 1000000);
            System.out.println("===");
            time = System.nanoTime();
            for (int i = 0; i < 10000; i++) {
                ClassUtils.getClassPathByMethod(getMethodName);
            }
            System.out.println(1.0 * (System.nanoTime() - time) / 1000000);
            String classPathByMethod = ClassUtils.getClassPathByMethod(getMethodName);
            System.out.println(classPathByMethod);

            System.out.println(getMethodName.getName());
            System.out.println(getMethodName.getDeclaringClass().getSimpleName());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
