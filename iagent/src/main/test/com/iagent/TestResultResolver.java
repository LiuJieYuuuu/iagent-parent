package com.iagent;

import com.iagent.factory.DefaultIagentFactory;
import com.iagent.factory.IagentConfiguration;
import com.iagent.factory.IagentFactory;
import com.iagent.resovler.result.ResultResolver;
import com.iagent.util.ClassUtils;
import com.iagent.util.ReflectUtils;
import org.junit.Test;

import java.util.List;

/**
 * @auther liujieyu
 * @date 2022/12/3
 * @description 测试结果解析器
 * @since 
 */
public class TestResultResolver {

    @Test
    public void context() {
        IagentConfiguration configuration = new IagentConfiguration("com.iagent");

        List<Class<ResultResolver>> aliasClassListByClass = configuration.getAliasRegister().getAliasClassListByClass(ResultResolver.class);
        for (int i = 0; i < aliasClassListByClass.size(); i++) {
            Class<ResultResolver> resultResolverClass = aliasClassListByClass.get(i);
            ResultResolver resultResolver = ClassUtils.newInstance(resultResolverClass);
            ReflectUtils.setFieldValue(resultResolver, "configuration", configuration);
            System.out.println(resultResolver);
        }
    }

}
