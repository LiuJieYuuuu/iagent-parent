package com.iagent.annotation;

import java.lang.annotation.*;

/**
 * Path parameter annotation
 *
 * @auther liujieyu
 * @date 2022/12/17
 * @description
 * @since
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.PARAMETER})
@Documented
public @interface PathKey {

    /**
     * value is key
     * @return
     */
    String value();

}
