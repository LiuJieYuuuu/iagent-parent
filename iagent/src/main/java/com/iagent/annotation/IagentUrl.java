package com.iagent.annotation;

import com.iagent.constant.HttpEnum;
import com.iagent.request.HttpExecutor;
import com.iagent.request.apache.ApacheSingleHttpClientExecutor;

import java.lang.annotation.*;

/**
 * <p>create iagent proxy</p>
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD,ElementType.TYPE})
@Documented
public @interface IagentUrl {

    /**
     * value is url
     * @return
     */
    String value() default "";

    HttpEnum[] requestType() default {};

    String contentType() default "";

    int connectionTime() default -1;

    int readTime() default -1;

    /**
     * default use to ApacheSingleHttpClientExecutor.java
     * @return
     */
    Class<? extends HttpExecutor> httpExecutor() default ApacheSingleHttpClientExecutor.class;

}
