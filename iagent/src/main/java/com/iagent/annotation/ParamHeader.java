package com.iagent.annotation;

import java.lang.annotation.*;

/**
 * Request header parameter annotation
 *
 * @author liujieyu
 * @date 2022/5/16 21:15
 * @desciption Request header parameter annotation
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.PARAMETER})
@Documented
public @interface ParamHeader {

    String value();

}
