package com.iagent.annotation;

import com.iagent.constant.HttpEnum;
import com.iagent.request.HttpExecutor;
import com.iagent.request.apache.ApacheSingleHttpClientExecutor;

import java.lang.annotation.*;

/**
 * quick using
 *
 * @author liujieyu
 * @since 2.1.0
 * @see IagentUrl
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
@Documented
@IagentUrl(requestType = HttpEnum.POST)
public @interface PostUrl {

    /**
     * value is url
     * @return
     */
    String value() default "";

    String contentType() default "";

    int connectionTime() default -1;

    int readTime() default -1;

    /**
     * default use to ApacheSingleHttpClientExecutor.java
     * @return
     */
    Class<? extends HttpExecutor> httpExecutor() default ApacheSingleHttpClientExecutor.class;
}
