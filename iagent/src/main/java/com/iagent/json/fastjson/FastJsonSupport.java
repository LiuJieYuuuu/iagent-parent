package com.iagent.json.fastjson;

import com.alibaba.fastjson.JSON;
import com.iagent.exception.JsonException;
import com.iagent.json.JSONSupport;
import com.iagent.logging.LogFactory;
import com.iagent.logging.Logger;
import com.iagent.util.ClassUtils;

/*
 * fastjson support
 */
public class FastJsonSupport implements JSONSupport {

    private Logger logger = LogFactory.getLogger(FastJsonSupport.class);

    private final String CLASS_NAME = "com.alibaba.fastjson.JSON";

    public FastJsonSupport() {
        super();
        try {
            Class<?> clazz = ClassUtils.getClassLoader().loadClass(CLASS_NAME);
            if (clazz == null) {
                clazz = Class.forName(CLASS_NAME);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Using Fastjson adapter framework");
            }
        } catch (Throwable t) {
            throw new JsonException("Not found alibaba fastjson framework in project");
        }
    }

    @Override
    public <T> T getJSONObject(String text, Class<T> clazz) {
        return JSON.parseObject(text, clazz);
    }

    @Override
    public String toJSONString(Object object) {
        return com.alibaba.fastjson.JSON.toJSONString(object);
    }

}
