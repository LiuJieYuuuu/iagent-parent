package com.iagent.register;


/**
 * Public registry interface
 *
 * @auther liujieyu
 * @date 2022/12/17
 * @description
 * @since 2.0.0
 */
public interface BeanRegister<T> {

    /**
     * Register it in a container
     *
     * @param beanName key
     * @param target value
     * @return
     */
    boolean registerBean(String beanName, T target);

    /**
     * Retrieved from the container by name, the more different classes are loaded into different objects
     *
     * @param beanName
     * @param tClass
     * @param <V>
     * @return
     */
    <V> V getBeanByClass(String beanName, Class<V> tClass);

    /**
     * Get the Object object by name
     *
     * @param beanName
     * @return
     */
    T getBeanObject(String beanName);

    /**
     * Whether to include the name of the bean
     *
     * @param beanName
     * @return
     */
    boolean containBeanName(String beanName);

    /**
     * Delete the data in the container by name
     *
     * @param beanName
     * @return
     */
    boolean removeBean(String beanName);

    /**
     * Gets all the registered names
     *
     * @return
     */
    String[] getAllBeanNames();

    /**
     * get size
     *
     * @return
     */
    int size();

    /**
     * remove all data
     */
    void removeAll();
}
