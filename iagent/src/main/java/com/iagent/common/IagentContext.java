package com.iagent.common;

import com.iagent.bean.IagentHeader;

/**
 * The iagent context sets the request header
 *
 * @author liujieyu
 * @date 2022/9/20 15:51
 * @desciption The iagent context sets the request header
 * @since 2.1.0
 */
public class IagentContext {

    private static ThreadLocal<IagentHeader> threadLocal = new ThreadLocal<IagentHeader>(){
        @Override
        protected IagentHeader initialValue() {
            return new IagentHeader();
        }
    };

    public static void setHeader(String key, Object value) {
        threadLocal.get().set(key, value);
    }

    public static IagentHeader getHeader() {
        return threadLocal.get();
    }

    public static void remove() {
        threadLocal.remove();
    }

    public static void removeKey(String key) {
        threadLocal.get().remove(key);
    }
}
