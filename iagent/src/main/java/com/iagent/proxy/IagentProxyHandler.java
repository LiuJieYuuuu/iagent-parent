package com.iagent.proxy;

import com.iagent.bean.IagentBeanWrapper;
import com.iagent.exception.HttpExecutorException;
import com.iagent.factory.IagentConfiguration;
import com.iagent.logging.LogFactory;
import com.iagent.logging.Logger;
import com.iagent.util.ClassUtils;

import java.lang.reflect.Method;

/**
 * Interface agent processor
 * @author liujieyu
 * @date 2022/12/17
 * @description
 * @since 1.0.0
 */
public class IagentProxyHandler {

    private static final Logger logger = LogFactory.getLogger(IagentProxyHandler.class);

    private IagentConfiguration configuration;

    public IagentProxyHandler(IagentConfiguration configuration) {
        this.configuration = configuration;
    }

    public Object invokeMethod(Method method, Object[] args) {
        IagentBeanWrapper wrapper = this.configuration.getBeanWrapperRegister().getBeanObject(String.valueOf(method.hashCode()));
        try {
            // Execute the request and return the result
            return wrapper.getExecutor().sendHttp(wrapper, args);
        } catch (Throwable throwable) {
            logger.error("The Method [" + ClassUtils.getClassPathByMethod(method) + "] Request Error:" + throwable.getMessage(), throwable);
            throw new HttpExecutorException(throwable);
        }
    }

}
