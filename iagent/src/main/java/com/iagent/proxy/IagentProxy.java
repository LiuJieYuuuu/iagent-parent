package com.iagent.proxy;

import com.iagent.factory.IagentConfiguration;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * <p>dynamic proxy implementation class ,it's InvocationHandler Class</p>
 *
 * @see IagentProxyFactory
 */
public class IagentProxy implements InvocationHandler {

    private IagentConfiguration configuration;

    public IagentProxy(IagentConfiguration configuration){
        this.configuration = configuration;
    }

    /**
     * <p>Dynamic Proxy real run function</p>
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if(Object.class.equals(method.getDeclaringClass())){
            // The methods of the object class run directly
            return method.invoke(this,args);
        }else{
            // That is, the interface proxy method
            return this.configuration.getProxyHandler().invokeMethod(method, args);
        }
    }

}
