package com.iagent.resovler.result;

/**
 * @author liujieyu
 * @date 2022/11/18 19:10
 * @desciption 抽象执行结果解析器
 * @since 2.2.0
 */
public interface ResultResolver {

    /**
     * 判断是否使用当前解析器进行解析数据
     *
     * @param returnClass 接口返回对象Class对象
     * @return
     */
    boolean isResolver(Class<?> returnClass);

    /**
     * 将返回结果按照return类型进行转换
     *
     * @param data 执行结果返回的字节数组
     * @param returnClass 接口返回对象Class对象
     * @return 返回解析的对象
     */
    Object resolver(byte[] data, Class<?> returnClass) throws Throwable;

}
