package com.iagent.resovler.result;

import com.iagent.annotation.Order;
import com.iagent.constant.HttpConstant;
import com.iagent.json.JSON;

/**
 * @auther liujieyu
 * @date 2022/12/2
 * @description 结果解析器，解析对象
 * @since 2.2.0
 */
@Order(HttpConstant.MAX_ORDER)
public class ObjectResultResolver extends AbstractResultResolver{
    @Override
    public boolean isResolver(Class<?> returnClass) {
        return Object.class.isAssignableFrom(returnClass);
    }

    @Override
    public Object resolver(byte[] data, Class<?> returnClass) throws Throwable {
        return JSON.getJSONObject(new String(data), returnClass);
    }
}
