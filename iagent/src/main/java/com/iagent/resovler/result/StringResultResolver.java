package com.iagent.resovler.result;

import com.iagent.annotation.Order;
import com.iagent.constant.HttpConstant;

/**
 * @auther liujieyu
 * @date 2022/12/2
 * @description string类型结果解析器
 * @since 2.2.0
 */
@Order(HttpConstant.MAX_ORDER - 20)
public class StringResultResolver extends AbstractResultResolver{
    @Override
    public boolean isResolver(Class<?> returnClass) {
        return String.class.isAssignableFrom(returnClass);
    }

    @Override
    public Object resolver(byte[] data, Class<?> returnClass) {
        return new String(data);
    }
}
