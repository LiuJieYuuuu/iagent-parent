package com.iagent.resovler.result;


import com.iagent.factory.IagentConfiguration;
import com.iagent.util.RandomUtils;

import java.io.File;

/**
 * @auther liujieyu
 * @date 2022/12/2
 * @description Parameter result parser abstract class
 * @since
 */
public abstract class AbstractResultResolver implements ResultResolver {

    protected IagentConfiguration configuration;

    public AbstractResultResolver() {
        super();
    }

    protected void setConfiguration(IagentConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Generate the cache file path
     */
    protected String getCacheTmpFilePath() {
        return configuration.getCachePath() + File.separator + RandomUtils.getId();
    }
}
