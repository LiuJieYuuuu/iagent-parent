package com.iagent.resovler.parameter;

import com.iagent.factory.IagentConfiguration;
import com.iagent.util.RandomUtils;

import java.io.File;

/**
 * @auther liujieyu
 * @date 2022/12/6
 * @description abstract parameter resolver
 * @since
 */
public abstract class AbstractParameterResolver implements ParameterResolver {

    protected IagentConfiguration configuration;

    public AbstractParameterResolver() {
        super();
    }

    protected void setConfiguration(IagentConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Generate the cache file path
     */
    protected String getCacheTmpFilePath() {
        return configuration.getCachePath();
    }

}
