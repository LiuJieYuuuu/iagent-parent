package com.iagent.resovler.parameter;

/**
 * @author liujieyu
 * @date 2022/12/01 00:33
 * @desciption 抽象执行参数解析器
 * @since 2.2.0
 */
public interface ParameterResolver {

    /**
     * 将传递进来的初始参数解析成最终结果参数
     *
     * @param arg 接口调用传递参数
     * @return 返回已经解析好的参数结果参数
     */
    Object resolver(Object arg) throws Throwable;
}
