package com.iagent.factory;

/**
 * Abstract Iagent Factory
 * 便于后期扩展
 */
public abstract class AbstractIagentFactory implements IagentFactory {

    private final IagentConfiguration configuration;

    public AbstractIagentFactory() {
        super();
        // 默认扫描所有包下类, 但是不建议扫描所有，容易报错
        configuration = new IagentConfiguration(new String[]{""});
        this.configuration.init();
    }

    public AbstractIagentFactory(String[] basePackages) {
        super();
        configuration = new IagentConfiguration();
        this.configuration.setBasePackages(basePackages);
        this.configuration.init();
    }

    public AbstractIagentFactory(IagentConfiguration configuration) {
        super();
        this.configuration = configuration;
        this.configuration.init();
    }

    public IagentConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * 指定名称注册别名
     * @param name
     * @param clazz
     */
    public void registerAlias(String name, Class<?> clazz) {
        this.configuration.getAliasRegister().registerAlias(name, clazz);
    }

    /**
     * 使用全类名注册别名
     * @param clazz
     */
    public void registerAlias(Class<?> clazz) {
        this.registerAlias(clazz.getName(), clazz);
    }

    /**
     * refresh origin url
     * @since 2.1.0
     */
    public void refreshEnv() {
        this.configuration.getEnvironment().refreshOriginUrl(this.configuration.getBeanWrapperRegister());
    }

}
