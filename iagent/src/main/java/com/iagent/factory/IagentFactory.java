package com.iagent.factory;

/**
 * @author liujieyu
 * @date 2022/11/18 18:55
 * @desciption 抽象接口工厂
 * @since 2.2.0
 */
public interface IagentFactory {

    /**
     * 将某个接口实例化
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T getProxy(Class<T> clazz);

}
