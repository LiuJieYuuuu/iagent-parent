package com.iagent.core;

import com.iagent.factory.IagentConfiguration;
import com.iagent.bean.IagentBean;
import com.iagent.logging.LogFactory;
import com.iagent.logging.Logger;
import com.iagent.proxy.IagentProxyFactory;
import com.iagent.register.BeanRegister;
import com.iagent.core.kernel.AbstractResourceScanner;
import com.iagent.core.resolver.AbstractInterfaceAnnotationResolver;
import com.iagent.core.resolver.InterfaceAnnotationResolver;
import com.iagent.util.ClassUtils;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

/**
 * <b>interface is the must scanner,it's to be IagentProxyFactory and UriProxy</b>
 *
 * @see com.iagent.proxy.IagentProxyFactory
 * @see com.iagent.proxy.IagentProxy
 */
public final class ClassPathBeanScanner extends AbstractResourceScanner {

    private static final Logger logger = LogFactory.getLogger(ClassPathBeanScanner.class);

    /**
     * <b>use IagentProxyFactory to create proxy factory class</b>
     */
    private IagentProxyFactory proxyFactory;

    /**
     * <b>global configuration</b>
     */
    private IagentConfiguration configuration;

    /**
     * 注解处理器
     */
    private InterfaceAnnotationResolver annotationHandlers;

    /**
     * <b>this is global Configuration's Constructor</b>
     * @param configuration
     */
    public ClassPathBeanScanner(IagentConfiguration configuration){
        super();
        this.configuration = configuration;
        proxyFactory = new IagentProxyFactory(configuration);
        Class<AbstractInterfaceAnnotationResolver> alias = this.configuration.getAliasRegister().getAlias(this.configuration.getHandlerName());
        if (AbstractInterfaceAnnotationResolver.class.isAssignableFrom(alias)) {
            annotationHandlers = ClassUtils.newInstance(alias, new Class[]{IagentConfiguration.class}, new Object[]{this.configuration});
        }
    }

    protected InterfaceAnnotationResolver getPresentAnnotationHandlers() {
        return annotationHandlers;
    }

    /**
     * <b>scanner interface of base packages ,
     * and load to wrapper map </b>
     * @param iagentRegister
     * @param basePackages
     */
    public void scannerPackages(BeanRegister<Object> iagentRegister, String[] basePackages, List<Class> proxyClassList) {
        for(String packages : basePackages){
            if (logger.isDebugEnabled()) {
                logger.debug("load " + basePackages + " package");
            }
            // Get all the Class objects that match the criteria
            Set<Class> allClasses = this.findAllClassByClassPath(packages);
            for (Class aClass : allClasses) {
                // Register the proxy instance of the current CLass into the proxy instance container
                String classKey = ClassUtils.getClassPathByClass(aClass);
                iagentRegister.registerBean(classKey, proxyFactory.newInstance(aClass));
                if (logger.isDebugEnabled()) {
                    logger.debug("load class name [" + classKey + "] the instance is " + iagentRegister.getBeanObject(classKey));
                }
                // Register each interface method in the wrapper class and register the actuator in the actuator container
                registerMethod(aClass);

                // Save the ordered collection of all scanned classes
                proxyClassList.add(aClass);
            }

        }
        // Replace the parameters in the Environment
        configuration.getEnvironment().refreshOriginUrl(configuration.getBeanWrapperRegister());
    }

    /**
     * <b>handle annotation of interface is loading to IagentBeanWrapper </b>
     * @param cls
     * @param <T>
     */
    private <T> void registerMethod(Class<T> cls){
        InterfaceAnnotationResolver presentAnnotationHandlers = getPresentAnnotationHandlers();
        //处理类上注解，拿到统一基本信息
        IagentBean iagentBean = presentAnnotationHandlers.handlerClassIagentBean(cls);
        if (iagentBean == null) {
            logger.warn("the class [" + cls + "] not has annotation IagentUrl");
            return;
        }
        //处理每个接口上的注解
        for (Method method : getDeclaredMethods(cls)) {
            if (presentAnnotationHandlers.existsIagentAnnotation(method)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("the class [" + cls + "] has handled the request method [" + ClassUtils.getClassPathByMethod(method) + "]");
                }
                presentAnnotationHandlers.handlerInterfaceMethod(iagentBean, method);
            }
        }

    }

    /**
     * 获取Class的所有方法
     * @param cls
     * @return
     */
    private Method[] getDeclaredMethods(Class<?> cls) {
        return cls.getMethods();
    }
}
