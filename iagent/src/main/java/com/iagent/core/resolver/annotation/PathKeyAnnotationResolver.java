package com.iagent.core.resolver.annotation;

import com.iagent.annotation.PathKey;
import com.iagent.bean.IagentParamBean.IagentParamBeanBuilder;

import java.lang.reflect.Parameter;

/**
 * path key annotation resolver
 */
public class PathKeyAnnotationResolver implements AnnotationResolver {

    @Override
    public boolean isResolver(Parameter parameter) {
        return parameter.getAnnotation(PathKey.class) != null;
    }

    @Override
    public void handleAnnotation(Parameter parameter, IagentParamBeanBuilder builder, int index) {
        PathKey annotation = parameter.getAnnotation(PathKey.class);
        String name = annotation.value();
        builder.pathIndex(name, index);
    }

}
