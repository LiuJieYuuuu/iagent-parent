package com.iagent.core.resolver.annotation;

import com.iagent.annotation.ParamBody;
import com.iagent.bean.IagentParamBean.IagentParamBeanBuilder;

import java.lang.reflect.Parameter;

/**
 * request body annotation resolver
 */
public class BodyAnnotationResolver implements AnnotationResolver {

    @Override
    public boolean isResolver(Parameter parameter) {
        return parameter.getAnnotation(ParamBody.class) != null;
    }

    @Override
    public void handleAnnotation(Parameter parameter, IagentParamBeanBuilder builder, int index) {
        builder.bodyIndex(index);
    }

}
