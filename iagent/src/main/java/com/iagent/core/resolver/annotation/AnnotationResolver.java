package com.iagent.core.resolver.annotation;

import com.iagent.bean.IagentParamBean.IagentParamBeanBuilder;

import java.lang.reflect.Parameter;

/**
 * @author liujieyu
 * @date 2022/5/31 10:45
 * @desciption Annotation parser
 * @since 2.2.0
 */
public interface AnnotationResolver {

    /**
     * 是否能解析
     * @param parameter
     * @return
     */
    boolean isResolver(Parameter parameter);

    /**
     * 参数处理封装
     * @param parameter
     * @param builder
     */
    void handleAnnotation(Parameter parameter, IagentParamBeanBuilder builder, int index);

}
