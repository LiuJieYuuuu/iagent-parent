package com.iagent.core.resolver.annotation;

import com.iagent.annotation.ParamHeader;
import com.iagent.bean.IagentParamBean;

import java.lang.reflect.Parameter;

/**
 * param header annotation resolver
 */
public class HeaderAnnotationResolver implements AnnotationResolver {
    @Override
    public boolean isResolver(Parameter parameter) {
        return parameter.getAnnotation(ParamHeader.class) != null;
    }

    @Override
    public void handleAnnotation(Parameter parameter, IagentParamBean.IagentParamBeanBuilder builder, int index) {
        ParamHeader annotation = parameter.getAnnotation(ParamHeader.class);
        String name = annotation.value();
        builder.headerIndex(name, index);
    }

}
