package com.iagent.core.resolver.annotation;

import com.iagent.annotation.ParamKey;
import com.iagent.bean.IagentParamBean;

import java.lang.reflect.Parameter;

/**
 * param key annotation resolver
 */
public class GenericAnnotationResolver implements AnnotationResolver {

    @Override
    public boolean isResolver(Parameter parameter) {
        return parameter.getAnnotation(ParamKey.class) != null;
    }

    @Override
    public void handleAnnotation(Parameter parameter, IagentParamBean.IagentParamBeanBuilder builder, int index) {
        ParamKey annotation = parameter.getAnnotation(ParamKey.class);
        String name = annotation.value();
        builder.paramIndex(name, index);
    }

}
