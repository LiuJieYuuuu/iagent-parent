package com.iagent.core.kernel.classreader;

import com.iagent.core.kernel.UrlResource;

/**
 * read class file by UrlResource
 * @see com.iagent.core.kernel.UrlResource
 */
public interface ClassReader {

    Class<?> getClassInfoByUrlResource(UrlResource urlResource);

}
