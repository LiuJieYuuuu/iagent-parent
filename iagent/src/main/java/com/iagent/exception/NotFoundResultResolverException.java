package com.iagent.exception;

/**
 * @auther Exception
 * @date 2022/12/2
 * @description 未找到结果参数解析器
 * @since 2.2.0
 */
public class NotFoundResultResolverException extends RuntimeException {
    public NotFoundResultResolverException() {
        super();
    }

    public NotFoundResultResolverException(String message) {
        super(message);
    }

    public NotFoundResultResolverException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundResultResolverException(Throwable cause) {
        super(cause);
    }
}
