package com.iagent.exception;

/**
 * http executor execute fail
 *
 * @auther liujieyu
 * @date 2022/12/17
 * @description
 * @since 2.2.0
 */
public class HttpExecutorException extends RuntimeException{

    public HttpExecutorException() {
        super();
    }

    public HttpExecutorException(String message) {
        super(message);
    }

    public HttpExecutorException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpExecutorException(Throwable cause) {
        super(cause);
    }

    public HttpExecutorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
