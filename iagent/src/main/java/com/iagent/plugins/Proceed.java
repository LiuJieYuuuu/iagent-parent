package com.iagent.plugins;

import java.lang.reflect.Method;

/**
 * Asserts, calling a method on the next link
 *
 * @author liujieyu
 * @description
 * @since 2.1.0
 * @see PluginChain
 */
public class Proceed {

    private Object proxy;

    private Method method;

    private Object[] args;

    public Proceed(Object proxy, Method method, Object[] args) {
        this.proxy = proxy;
        this.method = method;
        this.args = args;
    }

    /**
     * Continue executing the proxy method
     *
     * @return The result of execute
     * @throws Throwable
     */
    public Object proceed() throws Throwable {
        return method.invoke(proxy, args);
    }

}
