package com.iagent.plugins;

/**
 *
 * @author liujieyu
 * @description intercept http executor send http
 * @since 2.1.0
 */
public interface Interceptor {

    Object intercept(Proceed proceed) throws Throwable;

}
