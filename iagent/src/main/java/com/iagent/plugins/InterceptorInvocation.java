package com.iagent.plugins;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * JDK 动态代理实现代理类
 * @author liujieyu
 * @since 2.1.0
 * @see PluginChain
 */
public class InterceptorInvocation implements InvocationHandler {

    /**
     * 具体执行对象
     * target object like HttpExecutor/Interceptor
     */
    private Object target;

    /**
     * 具体拦截对象
     * proxy object
     */
    private Interceptor interceptor;

    public InterceptorInvocation(Object target, Interceptor interceptor) {
        this.target = target;
        this.interceptor = interceptor;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if(Object.class.equals(method.getDeclaringClass())){
            //object 类的方法直接运行即可
            return method.invoke(this,args);
        }else{
            // 其他执行方法走拦截器代理
            return interceptor.intercept(new Proceed(target, method, args));
        }
    }
}
