package com.iagent.plugins.plugin;

import com.iagent.annotation.Order;
import com.iagent.logging.LogFactory;
import com.iagent.logging.Logger;
import com.iagent.plugins.Interceptor;
import com.iagent.plugins.Proceed;

/**
 * @author liujieyu
 * @description Gets the HttpExecutor execution time plugin
 * @since 2.1.0
 */
@Order
public class ExecuteTimePlugin implements Interceptor {

    private static final Logger logger = LogFactory.getLogger(ExecuteTimePlugin.class);

    @Override
    public Object intercept(Proceed proceed) throws Throwable {
        if (logger.isDebugEnabled()) {
            long startTime = System.nanoTime();
            Object object = proceed.proceed();
            logger.debug("the execute time is " + 1.0 * (System.nanoTime() - startTime)/1000000 + " ms");
            return object;
        }
        return proceed.proceed();
    }
}
