package com.iagent.constant;

/**
 * <p>enum request method type</p>
 */
public enum HttpEnum {
    GET,
    POST,
    PUT,
    DELETE
}
