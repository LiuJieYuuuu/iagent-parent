package com.iagent.request.apache;

import com.iagent.logging.LogFactory;
import com.iagent.logging.Logger;
import com.iagent.request.RequestConfig;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author liujieyu
 * @date 2022/12/16 18:31
 * @desciption use pool client manager for http client
 */
public class ApachePoolHttpClientExecutor extends AbstractApacheHttpExecutor {

    private static final Logger logger = LogFactory.getLogger(ApachePoolHttpClientExecutor.class);

    // Number of all hosts in the connection pool
    private static final int MAX_TOTAL = 200;
    // Maximum number of connections to a single host
    private static final int DEFAULT_MAX_PER_ROUTE = 20;
    // In concurrent cases, objects are prevented from being created repeatedly
    private final Lock lock = new ReentrantLock();
    // route pool
    private final Map<String, CloseableHttpClient> clientPool = new ConcurrentHashMap<>();
    // apache client request config
    private Map<String, org.apache.http.client.config.RequestConfig> configPool = new ConcurrentHashMap<>();

    @Override
    public CloseableHttpClient getHttpClient(String url, RequestConfig config) throws UnsupportedOperationException {
        String httpRoute = getHttpRoute(url);
        if (clientPool.containsKey(httpRoute)) {
            return clientPool.get(httpRoute);
        } else {
            lock.lock();
            try {
                // Ease the stress of creating objects
                if (clientPool.containsKey(httpRoute)) {
                    return clientPool.get(httpRoute);
                }
                CloseableHttpClient httpClient = createCloseableHttpClient(config);
                clientPool.put(httpRoute, httpClient);
                return httpClient;
            } finally {
                lock.unlock();
            }
        }
    }

    @Override
    public void releaseConnection(CloseableHttpClient httpClient) throws UnsupportedOperationException {
        // do eny nothing
    }

    /**
     * get url route for pool client manager
     *
     * @param url
     * @return
     */
    private String getHttpRoute(String url) {
        StringBuilder httpRoute = new StringBuilder();
        try {
            URI uri = new URI(url);
            httpRoute.append(uri.getScheme());
            httpRoute.append("://");
            httpRoute.append(uri.getHost());
            httpRoute.append(":");
            httpRoute.append(uri.getPort());
            return httpRoute.toString();
        } catch (URISyntaxException e) {
            logger.error("The uri is not an http link", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * create default PoolingHttpClientConnectionManager
     *
     * @return
     */
    private PoolingHttpClientConnectionManager createPoolingHttpClientConnectionManager() {
        PoolingHttpClientConnectionManager clientConnectionManager = new PoolingHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", getSslSocketFactory())
                        .build());
        clientConnectionManager.setMaxTotal(MAX_TOTAL);
        clientConnectionManager.setDefaultMaxPerRoute(DEFAULT_MAX_PER_ROUTE);
        return clientConnectionManager;
    }

    /**
     * create default CloseableHttpClient
     *
     * @param config
     * @return
     */
    private CloseableHttpClient createCloseableHttpClient(RequestConfig config) {
        String key = config.getConnectionTime() + String.valueOf(config.getReadTime());
        org.apache.http.client.config.RequestConfig requestConfig = null;
        if (configPool.containsKey(key)) {
            requestConfig = configPool.get(key);
        } else {
            requestConfig = org.apache.http.client.config.RequestConfig.custom()
                    .setConnectionRequestTimeout(config.getConnectionTime())
                    .setConnectTimeout(config.getConnectionTime())
                    .setSocketTimeout(config.getReadTime())
                    .build();
            configPool.put(key, requestConfig);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Create http client for pool manager");
        }
        return HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                // ignore ssl
                .setSSLSocketFactory(getSslSocketFactory())
                // set pool connection
                .setConnectionManager(createPoolingHttpClientConnectionManager())
                // set retry handler
                .setRetryHandler(new CommonHttpRequestRetryHandler())
                // close pool client share
                .setConnectionManagerShared(false)
                // start evict expired connection thread
                .evictExpiredConnections()
                .evictIdleConnections(2, TimeUnit.SECONDS)
                .build();
    }

}
