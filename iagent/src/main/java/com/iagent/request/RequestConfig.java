package com.iagent.request;

import com.iagent.constant.HttpConstant;
import com.iagent.constant.HttpEnum;

/**
 * default request configuration
 */
public class RequestConfig implements Cloneable{

    private HttpEnum requestType = HttpEnum.GET;

    private String contentType = HttpConstant.X_WWW_FORM_URLENCODED;

    private int connectionTime = HttpConstant.CONNECTION_TIME;

    private int readTime = HttpConstant.READ_TIME;

    private Class<? extends HttpExecutor> httpExecutor;

    /**
     * using default http executor
     */
    public RequestConfig() {
        super();
        this.httpExecutor = HttpExecutor.DEFAULT_EXECUTOR;
    }

    public RequestConfig(Class<? extends HttpExecutor> httpExecutor) {
        this.httpExecutor = httpExecutor;
    }

    public HttpEnum getRequestType() {
        return requestType;
    }

    public void setRequestType(HttpEnum requestType) {
        this.requestType = requestType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public int getConnectionTime() {
        return connectionTime;
    }

    public void setConnectionTime(int connectionTime) {
        this.connectionTime = connectionTime;
    }

    public int getReadTime() {
        return readTime;
    }

    public void setReadTime(int readTime) {
        this.readTime = readTime;
    }

    public Class<? extends HttpExecutor> getHttpExecutor() {
        return httpExecutor;
    }

    public void setHttpExecutor(Class<? extends HttpExecutor> httpExecutor) {
        this.httpExecutor = httpExecutor;
    }

    @Override
    protected RequestConfig clone() throws CloneNotSupportedException {
        return (RequestConfig) super.clone();
    }

    @Override
    public String toString() {
        return "{" +
                "requestType=" + requestType +
                ", contentType=" + contentType +
                ", connectionTime=" + connectionTime +
                ", readTime=" + readTime +
                ", httpExecutor=" + httpExecutor +
                '}';
    }
}
