package com.iagent.request;

import com.iagent.bean.IagentBeanWrapper;
import com.iagent.request.apache.ApacheSingleHttpClientExecutor;

/**
 * <b>Common Http Request Interface</b>
 */
public interface HttpExecutor {

    /**
     * default executor
     *
     */
    Class<? extends HttpExecutor> DEFAULT_EXECUTOR = ApacheSingleHttpClientExecutor.class;

    /**
     * specific http request
     * @param bean
     * @param args
     * @return
     */
    Object sendHttp(IagentBeanWrapper bean, Object[] args) throws Throwable;

}
