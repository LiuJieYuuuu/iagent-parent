package com.iagent.bean;

/**
 * @author liujieyu
 * @description banner printer
 * @since 2.1.1
 */
public class BannerPrinter {

    private final String BANNER_INFO = "  _____               _____   ______   _   _   _______ \n" +
            " |_   _|     /\\      / ____| |  ____| | \\ | | |__   __|\n" +
            "   | |      /  \\    | |  __  | |__    |  \\| |    | |   \n" +
            "   | |     / /\\ \\   | | |_ | |  __|   | . ` |    | |   \n" +
            "  _| |_   / ____ \\  | |__| | | |____  | |\\  |    | |   \n" +
            " |_____| /_/    \\_\\  \\_____| |______| |_| \\_|    |_|   \n";

    /**
     * Print or not
     * default is true
     */
    private boolean mode = true;

    public BannerPrinter() {
    }

    public BannerPrinter(boolean mode) {
        this.mode = mode;
    }

    public void setMode(boolean mode) {
        this.mode = mode;
    }

    /**
     * 打印Banner信息
     */
    public void printerBanner() {
        if (mode) {
            System.out.println(BANNER_INFO);
        }
    }

}
