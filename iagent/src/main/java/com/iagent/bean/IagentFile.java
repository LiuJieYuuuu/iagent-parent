package com.iagent.bean;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * the order of acquisition
 * the first one I get is content
 * the next thing you get is inputStream
 * the last on get is file
 *
 * @author liujieyu
 * @date 2022/12/9 16:42
 * @desciption the IagentFile Object is aiming at parameter of file type
 * @since 2.2.0
 */
public class IagentFile {

    // file name
    private String name;

    // file content
    private List<byte[]> content;

    // file InputStream
    private List<InputStream> inputStream;

    // file
    private List<File> file;

    public String getName() {
        return name;
    }

    public List<byte[]> getContent() {
        return content;
    }

    public List<InputStream> getInputStream() {
        return inputStream;
    }

    public List<File> getFile() {
        return file;
    }

    private IagentFile() {
        super();
    }

    public static IagentFileBuilder createBuilder() {
        return new IagentFileBuilder();
    }

    public static class IagentFileBuilder {
        private IagentFile iagentFile = new IagentFile();

        private IagentFileBuilder() {
            super();
        }

        public IagentFileBuilder name(String name) {
            iagentFile.name = name;
            return this;
        }

        public IagentFileBuilder content(byte[] data) {
            if (iagentFile.content == null) {
                List<byte[]> contentList = new ArrayList<>();
                contentList.add(data);
                iagentFile.content = contentList;
            } else {
                iagentFile.content.add(data);
            }
            return this;
        }

        public IagentFileBuilder file(File file) {
            if (iagentFile.file == null) {
                List<File> fileList = new ArrayList<>();
                fileList.add(file);
                iagentFile.file = fileList;
            } else {
                iagentFile.file.add(file);
            }
            return this;
        }

        public IagentFileBuilder inputStream(InputStream inputStream) {
            if (iagentFile.inputStream == null) {
                List<InputStream> inputStreams = new ArrayList<>();
                inputStreams.add(inputStream);
                iagentFile.inputStream = inputStreams;
            } else {
                iagentFile.inputStream.add(inputStream);
            }
            return this;
        }

        public IagentFile build() {
            return iagentFile;
        }
    }

}
