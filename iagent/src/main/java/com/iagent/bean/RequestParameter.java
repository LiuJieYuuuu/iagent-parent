package com.iagent.bean;

import com.iagent.common.IagentContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liujieyu
 * @date 2022/11/18 19:46
 * @desciption Request parameter set
 * @since 2.2.0
 */
public class RequestParameter {

    /**
     * 请求头参数集合，将@ParamHeader以及IagentContext.getHeader()进行汇总
     *
     * @see com.iagent.common.IagentContext
     * @see com.iagent.annotation.ParamHeader
     */
    private Map<String, Object> headers;

    /**
     * url路径参数集合以及表单参数集合（仅限于Content-Type: multipart/form-data）
     *
     * @see com.iagent.annotation.ParamKey
     */
    private Map<String, Object> form;

    /**
     * 将标记为body数据存放
     *
     * @see com.iagent.annotation.ParamBody
     */
    private Object body;

    /**
     * Add extension parameter
     *
     * @since 2.2.1
     */
    private List<Object> extendedParameter;

    /**
     * 通过Builder建造者进行创建
     */
    private RequestParameter() {
        super();
    }

    private void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    private void setForm(Map<String, Object> form) {
        this.form = form;
    }

    private void setBody(Object body) {
        this.body = body;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public Map<String, Object> getForm() {
        return form;
    }

    public Object getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "RequestParameter{" +
                "headers=" + headers +
                ", form=" + form +
                ", body=" + body +
                '}';
    }

    /**
     * 创建Builder
     */
    public static RequestParameterBuilder createBuilder() {
        return new RequestParameterBuilder();
    }

    /**
     * Request Parameter Builder
     */
    public static class RequestParameterBuilder {

        private RequestParameter requestParameter = new RequestParameter();

        public RequestParameterBuilder() {
            super();
        }

        public RequestParameterBuilder headers(Map<String, Integer> header, Object[] args) {
            Map<String, Object> data = new HashMap<>(8);
            if (header != null && !header.isEmpty()) {
                for (Map.Entry<String, Integer> entry : header.entrySet()) {
                    String name = entry.getKey();
                    Object value = args[entry.getValue().intValue()];
                    data.put(name, value);
                }
            }
            // get Iagent Context header info
            data.putAll(IagentContext.getHeader().get());
            if (!data.isEmpty()) {
                this.requestParameter.setHeaders(data);
            }
            // remove IagentContext data
            IagentContext.remove();
            return this;
        }

        public RequestParameterBuilder form(Map<String, Integer> form, Object[] args) {
            if (form != null && !form.isEmpty()) {
                Map<String, Object> data = new HashMap<>(8);
                for (Map.Entry<String, Integer> entry : form.entrySet()) {
                    String name = entry.getKey();
                    Object value = args[entry.getValue().intValue()];
                    data.put(name, value);
                }
                this.requestParameter.setForm(data);
            }
            return this;
        }

        public RequestParameterBuilder body(Integer body, Object[] args) {
            if (body != null) {
                this.requestParameter.setBody(args[body.intValue()]);
            }
            return this;
        }

        public RequestParameter build() {
            return requestParameter;
        }
    }

}
