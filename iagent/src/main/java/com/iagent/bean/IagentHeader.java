package com.iagent.bean;

import com.iagent.json.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liujieyu
 * @date 2022/9/20 15:46
 * @desciption Set the request header parameters synchronously
 * @since 2.1.0
 */
public class IagentHeader {

    private Map<String, Object> data = new HashMap<>(8);

    public IagentHeader() {
        super();
    }

    public void set(String key, Object value) {
        this.data.put(key, value);
    }

    public Map<String, Object> get() {
        return this.data;
    }

    public void remove(String key) {
        this.data.remove(key);
    }
    @Override
    public String toString() {
        return JSON.toJSONString(this.data);
    }

}
