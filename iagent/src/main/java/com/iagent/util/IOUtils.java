package com.iagent.util;

import com.iagent.logging.LogFactory;
import com.iagent.logging.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * File and Stream Utils
 */
public class IOUtils {

    private static final Logger logger = LogFactory.getLogger(IOUtils.class);
    /**
     * 将字节数组转换成文件
     * @param path
     * @param bytes
     */
    public static File writeToFileByBytes(String path, byte[] bytes) {
        File file = new File(path);
        OutputStream outputStream = null;
        try {
            if (!file.exists()) {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                file.createNewFile();

            }
            outputStream = new FileOutputStream(file);
            outputStream.write(bytes);
            outputStream.flush();
            return file;
        } catch (IOException e) {
            logger.error("IOUtils write bytes error", e);
            return null;
        } finally {
            if (null != outputStream) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
