package com.iagent.util;

import com.iagent.logging.LogFactory;
import com.iagent.logging.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * class util
 */
public class ClassUtils {

    private static final Logger logger = LogFactory.getLogger(ClassUtils.class);

    /**
     * get default class loader
     * @return
     */
    public static ClassLoader getClassLoader () {
        ClassLoader classLoader = null;
        try {
            classLoader = Thread.currentThread().getContextClassLoader();
        } catch (Throwable t) {
            //Cannot get thread context ClassLoader
        }
        if (classLoader == null) {
            // No thread context class loader -> use class loader of this class.
            classLoader = ClassUtils.class.getClassLoader();
            if (classLoader == null) {
                // getClassLoader() returning null indicates the bootstrap ClassLoader
                try {
                    classLoader = ClassLoader.getSystemClassLoader();
                }
                catch (Throwable ex) {
                    // Cannot access system ClassLoader - oh well, maybe the caller can live with null...
                }
            }
        }
        return classLoader;
    }

    /**
     * get CLass instance by class name and class have annotation
     * @param className
     * @param annotation
     * @return
     */
    public static Class getInterfaceClass(String className, Class annotation) {
        try {
            Assert.notNull(className, "ClassName is Null");
            Class<?> clazz = Class.forName(className);
            if (annotation == null && Modifier.isInterface(clazz.getModifiers())) {
                return clazz;
            } else if (Modifier.isInterface(clazz.getModifiers()) && clazz.getDeclaredAnnotation(annotation) != null) {
                return clazz;
            }
            return null;
        } catch (Throwable e) {
            logger.error("The [" + className + "] is Not Loader ");
        }
        return null;
    }

    /**
     * get class path #like com.iagent.util.ClassUtils
     * @param clazz
     * @return
     */
    public static String getClassPathByClass(Class clazz) {
        Assert.notNull(clazz, "Class is null");
        return clazz.getName();
    }

    /**
     * get Class Path of Method #like java.lang.Object.wait(long,int)
     * @param method
     * @return
     */
    public static String getClassPathByMethod(Method method) {
        Assert.notNull(method, "Method is Null");
        String classPath = getClassPathByClass(method.getDeclaringClass());
        Class<?>[] parameterTypes = method.getParameterTypes();
        StringBuilder builder = new StringBuilder();
        builder.append(classPath);
        builder.append(".");
        builder.append(method.getName());
        builder.append("(");
        for (Class<?> parameterType : parameterTypes) {
            builder.append(getClassPathByClass(parameterType));
            builder.append(",");
        }
        if (parameterTypes != null && parameterTypes.length > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        builder.append(")");
        return builder.toString();
    }

    /**
     * 使用构造函数创建对象
     * @param tClass
     * @param classes
     * @param args
     * @param <T>
     * @return
     */
    public static <T> T newInstance(Class<T> tClass, Class<?>[] classes, Object[] args) {
        try {
            Assert.notNull(classes, "The classes is not null");
            Assert.notNull(args, "The args is not null");
            if (classes.length != args.length) {
                throw new IllegalArgumentException("The number of parameters is not equal in the constructor");
            }
            Constructor<T> constructor = tClass.getConstructor(classes);
            T t = constructor.newInstance(args);
            return t;
        } catch (Throwable throwable) {
            logger.error("Cannot Load [" + tClass + "] Constructor, Error Message :", throwable);
            throw new IllegalArgumentException(throwable.getMessage());
        }
    }

    /**
     * create new instance
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T newInstance(Class<T> tClass) {
        try {
            return tClass.newInstance();
        } catch (Throwable throwable) {
            logger.error("Cannot Load [" + tClass + "] Constructor, Error Message :" + throwable.getMessage());
            throw new IllegalArgumentException(throwable.getMessage());
        }
    }

    /**
     * 返回指定函数的所有参数类型
     * @param method
     * @return
     */
    public static Class<?>[] getMethodParameterClass(Method method) {
        return method.getParameterTypes();
    }

    /**
     * 判断两个Class是否为同一个文件
     * @param source
     * @param target
     * @return
     * @since 2.1.0
     */
    public static boolean isEqualsClass(Class<?> source, Class<?> target) {
        return StringUtils.isEquals(ClassUtils.getClassPathByClass(source), ClassUtils.getClassPathByClass(target));
    }

    /**
     * determine whether the class exists
     * @param clazzName
     * @return
     * @since 2.1.0
     */
    public static boolean existsClassName(String clazzName) {
        try {
            Class.forName(clazzName);
            return true;
        } catch (Throwable throwable) {
            return false;
        }
    }

    /**
     * 获取Method 上的所有注解
     * @param method
     * @return
     */
    public static Annotation[] getAnnotations(Method method) {
        return method.getAnnotations();
    }

    /**
     * Get the class name of the method and method name
     *
     * @param method
     * @return like ClassUtils#getMethodName
     * @since 2.2.1
     */
    public static String getMethodName(Method method) {
        Assert.notNull(method, "Method is null");
        StringBuilder builder = new StringBuilder();
        builder.append(method.getDeclaringClass().getSimpleName());
        builder.append("#");
        builder.append(method.getName());
        return builder.toString();
    }
}
