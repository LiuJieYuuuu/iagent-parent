package com.iagent.util;

/**
 * @author liujieyu
 * @date 2022/5/13 19:13
 * @desciption Assertion utility class that assists in validating arguments.
 */
public final class Assert {

    public static void notNull(Object object, String message) {
        if (object == null) {
            throw new NullPointerException(message);
        }
    }

    public static void isInstanceOf(Class clazz, Object object) {
        notNull(clazz, "Check class instance of is not null");
        if (!clazz.isInstance(object)) {
            throw new IllegalArgumentException("Object of class [" + object.getClass() + "] must be an instance of " + clazz);
        }
    }

}
