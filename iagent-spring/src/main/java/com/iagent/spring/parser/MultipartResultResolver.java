package com.iagent.spring.parser;

import com.iagent.annotation.Order;
import com.iagent.constant.HttpConstant;
import com.iagent.resovler.result.AbstractResultResolver;
import com.iagent.util.RandomUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author liujieyu
 * @date 2022/9/16 20:51
 * @desciption For the MultipartFile implementation class under the Spring Web package
 */
@Order(HttpConstant.MAX_ORDER / 4)
public class MultipartResultResolver extends AbstractResultResolver {

    @Override
    public boolean isResolver(Class<?> returnClass) {
        return MultipartFile.class.isAssignableFrom(returnClass);
    }

    @Override
    public Object resolver(byte[] data, Class<?> returnClass) {
        return new MockMultipartFile(RandomUtils.getId(), data);
    }
}
