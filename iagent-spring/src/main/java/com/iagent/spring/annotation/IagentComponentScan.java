package com.iagent.spring.annotation;

import com.iagent.constant.HttpConstant;
import com.iagent.constant.HttpEnum;
import com.iagent.logging.Logger;
import com.iagent.logging.slf4j.Slf4jImpl;
import com.iagent.plugins.Interceptor;
import com.iagent.request.HttpExecutor;
import com.iagent.request.apache.ApacheSingleHttpClientExecutor;
import com.iagent.resovler.parameter.ParameterResolver;
import com.iagent.resovler.result.ResultResolver;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * <b>spring IDE scanner package</b>
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
@Import(value = {IagentImportBeanRegister.class})
public @interface IagentComponentScan {

    /**
     * value is packages
     * @return
     */
    String[] value();

    /**
     * using JSON adapter
     *
     * @return
     */
    String jsonSupport() default "";

    /**
     * Slf4j is used by default for processing using the specified logging framework
     *
     * @return
     */
    String logImpl() default "SLF4J";

    /**
     * Slf4j is used by default for processing using the specified logging framework
     *
     * @return
     */
    Class<? extends Logger> logImplClass() default Slf4jImpl.class;

    /**
     * Global default actuator
     * Only the first one is used by default
     *
     * @return
     */
    RequestConfig[] requestConfig() default {};

    /**
     * Default annotation handler, current version immutable
     *
     * @return
     */
    String resolver() default "Native";

    /**
     * add result parser
     * @since 1.1.0
     * @return
     */
    Class<? extends ResultResolver>[] addResultResolvers() default {};

    /**
     * add parameter resolver
     * @since 1.1.0
     * @return
     */
    Class<? extends ParameterResolver>[] addParameterResolvers() default {};

    /**
     * add interceptor class
     * @since 1.1.0
     * @return
     */
    Class<? extends Interceptor>[] plugins() default {};

    /**
     * The switch of banner printer
     * @since 1.1.1
     * @return
     */
    boolean bannerMode() default true;

    /**
     * The default configuration of the executor
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target({})
    @interface RequestConfig {

        /**
         * Default Request Type
         *
         * @return
         */
        HttpEnum requestType() default HttpEnum.GET;

        /**
         * Default Content-Type
         *
         * @return
         */
        String contentType() default HttpConstant.X_WWW_FORM_URLENCODED;

        /**
         * Default Connection Time
         *
         * @return
         */
        int connectionTime() default HttpConstant.CONNECTION_TIME;

        /**
         * Default read time
         *
         * @return
         */
        int readTime() default HttpConstant.READ_TIME;

        /**
         * default http executor
         *
         * @return
         */
        Class<? extends HttpExecutor> defaultHttpExecutor() default ApacheSingleHttpClientExecutor.class;

    }
}
