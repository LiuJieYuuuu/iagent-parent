package com.iagent.spring.annotation;

import com.iagent.constant.HttpEnum;
import com.iagent.plugins.Interceptor;
import com.iagent.request.HttpExecutor;
import com.iagent.request.RequestConfig;
import com.iagent.resovler.parameter.ParameterResolver;
import com.iagent.resovler.result.ResultResolver;
import com.iagent.spring.IagentProxyBean;
import com.iagent.spring.bind.IagentScannerConfigurer;
import com.iagent.util.ClassUtils;
import com.iagent.util.StringUtils;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * <p>iagent dependency in Spring IOC by Spring @Import</p>
 */
public class IagentImportBeanRegister implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        String proxyFactoryRef = generateBaseBeanName(importingClassMetadata, 0);
        Map<String, Object> annotationAttributes =
                importingClassMetadata.getAnnotationAttributes(IagentComponentScan.class.getName());
        // 获取基础配置
        RequestConfig requestConfig = EstablishRequestConfig((AnnotationAttributes[])annotationAttributes.get("requestConfig"));
        //if the ioc not have this bean
        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition(IagentProxyBean.class).getBeanDefinition();

        String[] packages = (String[]) annotationAttributes.get("value");
        if (packages != null && packages.length != 0) {
            beanDefinition.getPropertyValues().add("basePackages", packages);
        }

        String jsonSupport = (String) annotationAttributes.get("jsonSupport");
        if (StringUtils.isNotEmpty(jsonSupport)) {
            beanDefinition.getPropertyValues().add("jsonSupport", jsonSupport);
        }

        String logImpl = (String) annotationAttributes.get("logImpl");
        if (StringUtils.isNotEmpty(logImpl)) {
            beanDefinition.getPropertyValues().add("logImpl", logImpl);
        }

        String handleName = (String) annotationAttributes.get("resolver");
        if (StringUtils.isNotEmpty(handleName)) {
            // not to do something
        }

        Class<? extends ResultResolver>[] addResultResolvers = (Class<? extends ResultResolver>[]) annotationAttributes.get("addResultResolvers");
        if (addResultResolvers != null || addResultResolvers.length != 0) {
            beanDefinition.getPropertyValues().add("addResultResolvers", addResultResolvers);
        }

        Class<? extends ParameterResolver>[] addParameterResolvers = (Class<? extends ParameterResolver>[]) annotationAttributes.get("addParameterResolvers");
        if (addParameterResolvers != null || addParameterResolvers.length != 0) {
            beanDefinition.getPropertyValues().add("addParameterResolvers", addParameterResolvers);
        }

        Class<? extends Interceptor>[] plugins = (Class<? extends Interceptor>[]) annotationAttributes.get("plugins");
        if (plugins != null && plugins.length != 0) {
            beanDefinition.getPropertyValues().add("pluginClass", plugins);
        }

        boolean bannerMode = (boolean) annotationAttributes.get("bannerMode");
        beanDefinition.getPropertyValues().add("bannerMode", bannerMode);

        beanDefinition.getPropertyValues().add("requestConfig", requestConfig);
        registry.registerBeanDefinition(proxyFactoryRef, beanDefinition);

        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(IagentScannerConfigurer.class);
        beanDefinitionBuilder.addConstructorArgReference(proxyFactoryRef);
        registry.registerBeanDefinition(ClassUtils.getClassPathByClass(IagentScannerConfigurer.class), beanDefinitionBuilder.getBeanDefinition());
    }

    private String generateBaseBeanName(AnnotationMetadata importingClassMetadata, int index) {
        return importingClassMetadata.getClassName() + "#" + IagentProxyBean.class.getSimpleName() + "#" + index;
    }

    /**
     * 根据注解构建出一个RequestConfig对象
     * @see com.iagent.request.RequestConfig
     * @return
     */
    private RequestConfig EstablishRequestConfig(AnnotationAttributes[] annotationAttributes) {
        if (annotationAttributes.length == 0) {
            return new RequestConfig(HttpExecutor.DEFAULT_EXECUTOR);
        }
        int connectionTime = annotationAttributes[0].getNumber("connectionTime");
        String contentType = annotationAttributes[0].getString("contentType");
        Class<? extends HttpExecutor> defaultHttpExecutor = annotationAttributes[0].getClass("defaultHttpExecutor");
        int readTime = annotationAttributes[0].getNumber("readTime");
        HttpEnum requestType = annotationAttributes[0].getEnum("requestType");
        RequestConfig config = new RequestConfig(defaultHttpExecutor);
        config.setConnectionTime(connectionTime);
        config.setContentType(contentType);
        config.setReadTime(readTime);
        config.setRequestType(requestType);

        return config;
    }
}
